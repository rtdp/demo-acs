require 'test_helper'

class DocumentTest < ActiveSupport::TestCase

  test "to_param should return document name with url" do
    document = documents(:one)
    assert_equal "#{document.id}-ruby-on-rails", document.to_param
  end

  test "search rails should bring post with title Ruby on Rails as first" do 
    assert_equal Document.where(title: "Ruby on Rails").first.id, 
      Document.search("Ruby on Rails").first.id
  end

  test "search should prefer title over body" do 
    assert_equal Document.where(title: "Scala with Play").first.id, 
      Document.search("Scala").first.id
  end
end
