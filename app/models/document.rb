class Document < ActiveRecord::Base

  include PgSearch
  pg_search_scope :search, against: {
    title: 'A',
    body: 'B'
  },  using: {tsearch: {dictionary: 'english'}}

  # make urls seo friendly
  def to_param
    "#{id}-#{self.title.parameterize}"
  end
end
